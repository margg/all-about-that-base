require 'sequel'
require 'pry'
require 'faker'

DB = Sequel.connect('postgres://ukasiupass:password@localhost/pbd-ukasiu')
#DB = Sequel.connect('postgres://ukasiupass:hsk73uiz@mints.ukasiu.pl/pbd-ukasiu')
N = 10
ENABLE_DEBUG_CONSOLE = true

class Conference < Sequel::Model
  one_to_many :conference_reservations
  one_to_many :conference_days
end

class ConferenceReservation < Sequel::Model
  many_to_one :conference
  many_to_one :client
  one_to_many :day_reservations
  one_to_many :payments
end

class ConferenceDay < Sequel::Model
  one_to_many :price_ranges
  one_to_many :workshops
  one_to_many :day_reservations
  many_to_one :conference
end

class Client < Sequel::Model
  one_to_many :conference_reservations
end

class Payment < Sequel::Model
  many_to_one :conference_reservations
end

class Attendee < Sequel::Model
  one_to_many :day_attendees
end

class DayAttendee < Sequel::Model
  one_to_many :workshop_attendees
  many_to_one :day_reservations
end

class WorkshopAttendee < Sequel::Model
  many_to_one :workshop_reservations
  many_to_one :day_attendees
end
WorkshopAttendee.no_primary_key

class WorkshopReservation < Sequel::Model
  one_to_many :workshop_attendees
  many_to_one :day_reservation
  many_to_one :workshop
end

class Workshop < Sequel::Model
  many_to_one :conference_day
  one_to_many :workshop_reservations
end

class PriceRange < Sequel::Model
  many_to_one :conference_day
end

class DayReservation < Sequel::Model
  one_to_many :day_attendees
  one_to_many :workshop_reservations
  many_to_one :conference_reservation
  many_to_one :conference_day
end

binding.pry if ENABLE_DEBUG_CONSOLE

def prevent_from_duplicate(fun)
  @@dict ||= []
  loop do
    rand = fun.call
    break unless @@dict.include?(rand)
  end
  @@dict << rand
  rand
end

# individual clients
N.times do 
  Client.create(
    first_name: Faker::Name.first_name[0..49],
    last_name: Faker::Name.last_name[0..49],
    city: Faker::Address.city[0..49],
    street: Faker::Address.street_name[0..49],
    postal_code: Faker::Address.postcode[0..49],
    country: Faker::Address.country[0..49],
    number: Faker::Address.building_number[0..49],
    login: prevent_from_duplicate( -> { Faker::Internet.user_name[0..29] }),
    password_hash: Faker::Internet.password[0..49],
    email: prevent_from_duplicate( -> { Faker::Internet.email[0..49] }),
    phone_number: Faker::PhoneNumber.phone_number[0..19]
  )
end

# companies
N.times do 
  Client.create(
    company_name: Faker::Company.name[0..49],
    city: Faker::Address.city[0..49],
    street: Faker::Address.street_name[0..49],
    postal_code: Faker::Address.postcode[0..49],
    country: Faker::Address.country[0..49],
    number: Faker::Address.building_number[0..49],
    login: prevent_from_duplicate( -> { Faker::Internet.user_name[0..29] }),
    password_hash: Faker::Internet.password[0..49],
    email: prevent_from_duplicate( -> { Faker::Internet.email[0..49] }),
    phone_number: Faker::PhoneNumber.phone_number[0..19]
  )
end

# conferences
N.times do
  Conference.create(
    name: Faker::Lorem.sentence[0..299],
    venue_name: Faker::Lorem.sentence[0..49],
    venue_city: Faker::Address.city[0..49],
    venue_street: Faker::Address.street_name[0..49],
    venue_postal_code: Faker::Address.postcode[0..49],
    venue_country: Faker::Address.country[0..49],
    venue_number: Faker::Address.building_number[0..49],
    student_discount: Faker::Number.number(2)
  )
end

Conference.each do |conference|
  first_day_date = Faker::Date.between(Date.today - 365*4, Date.today + 100)
  person_limit = rand(100..400)
  rand(1..5).times do |index|
    cd = conference.add_conference_day(
      date: first_day_date + index,
      person_limit: person_limit
    )
    previous ||= first_day_date - rand(90..160)
    rand(1..3).times.map do |index|
      date = rand((previous+1)..first_day_date-(3-index))
      previous = date
    end.each do |date|
      cd.add_price_range(
        date_end: date,
        price: rand(100..1000)
      )
    end
    cd.add_price_range(
      date_end: first_day_date,
      price: rand(100..1000)
    )

    # workshops
    rand(0..12).times do 
      start_hour = rand(9..17)
      cd.add_workshop(
        name: Faker::Lorem.sentence[0..299],
        start_time: "#{start_hour}:#{%w( 00 15 30 45).sample}",
        end_time: "#{rand((start_hour+1)..18).to_s}:#{%w( 00 15 30 45).sample}",
        person_limit: rand(5..40),
        price: rand(50..400)
      )
    end
  end
end

free_places_conferences = {}
free_places_workshops = {}

Client.each do |client|
  conference_attendees = []
  conference_students = []

  rand(1..35).times do 
    conference_attendees << Attendee.create(
      first_name: Faker::Name.first_name[0..49],
      last_name: Faker::Name.last_name[0..49],
      email: Faker::Internet.email[0..49]
    )
    conference_students << conference_attendees.last if (rand(1..10) <= 5)
  end
  Conference.order(Sequel.lit('RANDOM()')).limit(rand(1..10)).each do |conference|
    conference_reservation = client.add_conference_reservation(
      conference: conference,
      created_at: conference.conference_days.first.date-rand(20..30)
    )

    ConferenceDay.order(Sequel.lit('RANDOM()')).limit(rand(1..conference.conference_days.count))
    .where(conference_id: conference.id).each do |cd|
      conference_day_attendees = []
      workshop_attendees = {}
      free_places_conferences[cd.id] ||= cd.person_limit
      conference_attendees.sample(rand(1..conference_attendees.count)).each do |attendee|
        if free_places_conferences[cd.id] > 0
          conference_day_attendees << attendee
          free_places_conferences[cd.id] -= 1
        end
      end

      if conference_day_attendees.count > 0 
        dr = conference_reservation.add_day_reservation(
          conference_day: cd,
          attendee_count: conference_day_attendees.count,
          student_count: conference_day_attendees.select {|a| conference_students.include?(a) }.count
        )
        conference_day_attendees.each do |at|
          if conference_students.include?(at)
            da = dr.add_day_attendee(
              attendee_id: at.id,
              transcript_number: Faker::Number.number(6)
            )
          else
            da = dr.add_day_attendee(
              attendee_id: at.id
            )
          end
          new_workshop_hour = 0
          rand(0..10).times do 
            Workshop.order(Sequel.lit('RANDOM()')).limit(1).where(conference_day: cd)
            .where('date_part(\'hour\',start_time) >= ?',new_workshop_hour).each do |w|
              free_places_workshops[w] ||= w.person_limit
              workshop_attendees[w] ||= []

              workshop_attendees[w] << da if free_places_workshops[w] > 0
              free_places_workshops[w] -= 1 if free_places_workshops[w] > 0
              new_workshop_hour = w.end_time.hour + 1
            end
          end
        end # conference_day_attendees.each
        workshop_attendees.each do |w,ats|
          next if ats.count == 0
          wr = dr.add_workshop_reservation(
            workshop: w,
            attendee_count: ats.count,
            student_count: ats.select {|a| a.transcript_number }.count
          )
          ats.each do |da|
            wa = da.add_workshop_attendee(
              workshop_reservation_id: wr.id
            )
          end
        end
      end
    end

    reservation_cost =  DB["select count_conference_reservation_cost(#{conference_reservation.id})"]
      .all.first.first[1]
    reservation_cost *= rand((0.5)..(1.5)) if rand < 1.0/5
    conference_reservation.add_payment(
      amount: reservation_cost
    )
  end # conference
end # clients

binding.pry if ENABLE_DEBUG_CONSOLE