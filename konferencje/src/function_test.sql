select add_conference('Testowa konferencja',10,'ice','Kraków','Monte Cassino','Polska','33-100','10');

select add_conference_day(1,'2015-04-01',10);
--select add_conference_day(1,'2015-04-01',10); --should fail
select add_conference_day(1,'2015-04-02',10);

select add_price_range('2015-03-31',100,1);
select add_price_range('2015-03-31',100,2);

select add_client('Łukasz','Gurdek',null,'Kraków','Konarskiego','Polska','30-049','10/2','ukasiu','blebleble');

select add_conference_reservation(1,1);
select add_day_reservation(1,1,10);
select add_attendee('Łukasz','Gurdek');

select add_normal_attendee_to_day_reservation(1,1);
select count_conference_reservation_cost(1);
select count_day_reservation_cost(1);

select add_workshop(1,'Testowe warsztaty','10:00','12:00',120,100);
select add_workshop(1,'Testowe warsztaty2','12:00','13:00',120,100);
select add_workshop(1,'Testowe warsztaty3','11:00','13:00',120,100);


select add_workshop_reservation(1,1,10);
select add_workshop_reservation(2,1,10);
select count_workshop_reservation_cost(1,0);
select count_workshop_reservation_cost(2,0);

select add_attendee_to_workshop_reservation(1,1);
select count_workshop_reservation_cost(1,0);
select count_workshop_reservation_cost(2,0);

select add_attendee_to_workshop_reservation(1,2);
select add_attendee_to_workshop_reservation(1,3);

select count_workshop_reservation_cost(1,0);
select count_day_reservation_cost(1);

select count_conference_reservation_cost(1);

select add_payment(1,NOW(),120.0);

select sum_payments_for_conference_reservation(1);